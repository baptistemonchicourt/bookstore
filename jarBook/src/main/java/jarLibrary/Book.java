package jarLibrary;


import net.tncy.validator.constraints.books.ISBN;

public class Book {
    private String title;
    private int release_date;
    @ISBN
    private String isbn;
    private boolean available;

    public Book(String title, int r_d, String isbn) {
        this.title = title;
        this.release_date = r_d;
        this.isbn = isbn;
        this.available = true;
    }

    public void toggleAvailable() {
        this.available = !this.available;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getRelease_date() {
        return release_date;
    }

    public void setRelease_date(int release_date) {
        this.release_date = release_date;
    }

    public String getIsbn() {
        return isbn;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

    public boolean isAvailable() {
        return available;
    }

    public void setAvailable(boolean available) {
        this.available = available;
    }
}
