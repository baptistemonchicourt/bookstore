import jarLibrary.Book;

public class BookServices {
    public void toggleBookAvailability(Book b){
        b.toggleAvailable();
    }
}
